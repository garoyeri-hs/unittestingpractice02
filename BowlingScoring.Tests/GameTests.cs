﻿namespace BowlingScoring.Tests
{
    using System.Threading.Tasks;
    using Features;
    using MediatR;
    using Microsoft.Extensions.DependencyInjection;
    using Shouldly;

    public class GameTests
    {
        private readonly Startup _game;
        private readonly IMediator _m;

        public GameTests()
        {
            _game = new Startup();
            _m = _game.Services.GetService<IMediator>();
        }

        public async Task ShouldStartWithZeroPoints()
        {
            var response = await _m.Send(new Score.Query());

            response.TotalScore.ShouldBe(0);
        }

        public async Task ShouldBeOneWhenPinDown()
        {
            await _m.Send(new Score.PinDown());
            var response = await _m.Send(new Score.Query());

            response.TotalScore.ShouldBe(1);
        }

        public async Task ShouldBeTwoWhenPinDownTwice()
        {
            await _m.Send(new Score.PinDown());
            await _m.Send(new Score.PinDown());
            var response = await _m.Send(new Score.Query());

            response.TotalScore.ShouldBe(2);
        }

        public void ShouldNotThrowAnything()
        {
            _game.Logger.ShouldNotBe(null);
        }

        public async Task ShouldPongOnPing()
        {
            var response = await _m.Send(new Ping.Command() {Message = "Hello!"});

            response?.Message.ShouldNotBeNull("Response message");
            response?.Message.ShouldBe("Hello!");
        }

        public async Task ShouldPongWithMessageOnPing()
        {
            var response = await _m.Send(new Ping.Command() {Message = "Howdy!"});

            response.ShouldNotBeNull("Response");
            response.Message.ShouldNotBeNull("Response message");
            response.Message.ShouldBe("Howdy!");
        }
    }
}
