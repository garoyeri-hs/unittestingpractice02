# Unit Test Brown Bag - Bowling with Mediatr

- Bowling Scoring: http://slocums.homestead.com/gamescore.html
- Setting up Mediatr: https://github.com/jbogard/MediatR/wiki
- (but with MS DI): https://github.com/jbogard/MediatR.Extensions.Microsoft.DependencyInjection
- Setting up Fixie: https://github.com/fixie/fixie/wiki#quick-start

- Uncle Bob, Bowling Kata: http://butunclebob.com/ArticleS.UncleBob.TheBowlingGameKata
  - (Describes it in step by step terms, see the PPT link)

