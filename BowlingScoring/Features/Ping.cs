﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BowlingScoring.Features
{
    using MediatR;

    public static class Ping
    {
        public class Command : IRequest<Pong>
        {
            public string Message { get; set; }
        }

        public class Handler : RequestHandler<Command, Pong>
        {
            protected override Pong Handle(Command request)
            {
                return new Pong { Message = request.Message };
            }
        }

        public class Pong
        {
            public string Message { get; set; }
        }
    }
}
