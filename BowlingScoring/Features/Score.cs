﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BowlingScoring.Features
{
    using Domain;
    using MediatR;

    public static class Score
    {
        public class Query: IRequest<Response>
        {
        }

        public class Handler : RequestHandler<Query, Response>
        {
            private readonly GameStatus _gameStatus;

            public Handler(GameStatus gameStatus)
            {
                _gameStatus = gameStatus;
            }

            protected override Response Handle(Query request)
            {
                return new Response
                {
                    TotalScore = _gameStatus.Score
                };
            }
        }

        public class Response
        {
            public int TotalScore { get; set; }
        }

        public class PinDown : IRequest
        {
        }

        public class PinDownHandler : RequestHandler<PinDown>
        {
            private readonly GameStatus _gameStatus;

            public PinDownHandler(GameStatus gameStatus)
            {
                _gameStatus = gameStatus;
            }

            protected override void Handle(PinDown request)
            {
                _gameStatus.Score++;
            }
        }
    }


}
