﻿namespace BowlingScoring
{
    using System;
    using Domain;
    using Features;
    using MediatR;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;

    public class Startup
    {
        public Startup()
        {
            IServiceCollection serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection);
            Services = serviceCollection.BuildServiceProvider();
            Logger = Services.GetRequiredService<ILoggerFactory>().CreateLogger<Startup>();
        }

        public IServiceProvider Services { get; }
        public ILogger Logger { get; }
       
        private void ConfigureServices(IServiceCollection serviceCollection)
        {
            serviceCollection.AddLogging();
            serviceCollection.AddMediatR(typeof(Startup).Assembly);
            serviceCollection.AddSingleton<GameStatus>();
        }

    }
}
